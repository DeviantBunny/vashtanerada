var express = require('express');
var router = express.Router();
import {getUser} from "../login";

// Homepage
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Vashta Nerada' });
});

// GET Search
router.get('/search/:id', function (req, res, next){
  res.render('search', {output: req.params.id, title: 'Search Results'})
});

// POST Search Submit
router.post('/search/submit', function (req, res, next){
  var id = req.body.id;
  res.redirect('/search/' + id);
})

// POST Login Submit
router.post('/login/submit', function (req, res, next){
  let credentials = {username: req.body.username, password: req.body.password};

  if (!credentials.username || !credentials.password){
    res.status(400).send("Username and Password fields are required.")
  }

  let user = login.getUser(credentials);
  if (user && user.password === credentials.password){
    res.send("Success")
  }
  else {
    res.send("Invalid Username or Password.")
  }

  //res.redirect('/users/' + credentials);
})



module.exports = router;
